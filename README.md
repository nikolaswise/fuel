# README

> Nanbox Wordpress Starter Pack

### Getting Started

```
$ git git@github.com:fuzzco/nanobox-wordpress.git project-name
$ npm run init
```

##  Development

```
$ npm start
```

### Adding DNS

```
$ nanobox dns add local your-site.local
$ npm start
```

### Building the Theme

0. Visit your your-site.local and finish the set up.
1. Enable the plugins needed.
2. Swith to the Fuzzco Theme is using Timber.
